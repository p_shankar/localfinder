""" Filters 
prototype: output_list = filter(latitudes, longitudes)

Filters should:
	1. Return list
	2. If return value is geoCode, return a REAL position.
	3. If return value is geoCode, return in tuple.
	4. Be either fast or meaningful.
"""
import time
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut

geolocator = Nominatim()

# Function Library
def geoCode2Country(lat, lng, results={}):
	global geolocator
	counter = 0
	location = None
	while location == None:
		if (lat, lng) in results.keys():
			print "Cached!",
			return results[(lat, lng)]
		try:
			location = geolocator.reverse("%f, %f" %(lat,lng))
			country_code = location.raw['address']['country_code']
			results[(lat, lng)] = country_code
		except GeocoderTimedOut:
			counter += 1
			if counter > 5:
				print "Retried 5 times. Failed."
				raise
			else:
				print "Time out. Retry(%s, %s)" %(lat, lng)
				time.sleep(0.5)
	return country_code

def isTaiwan(lat, lng):
	if lat > 25.32 or lat < 21.89:
		return False
	elif lng > 122.03 or lng < 119.3:
		return False
	else:
		return True

# Filters

def averageFilter(lats, lngs):
	try:
		avg_lat = sum(lats)/len(lats)
		avg_lng = sum(lngs)/len(lngs)
		return (avg_lat, avg_lng)
	except (ZeroDivisionError, TypeError):
		print "Invalid input."
		return ''

def voteOneCountryFilter(lats, lngs):
	countryCount = {}
	try:
		if len(lats) <> len(lngs):
			raise IndexError("len(latitudes) deos not equal to len(longitudes)")
	except TypeError:
		print "invalid input"
		return ''
		
	for x in xrange(len(lats)):
		try:
			country = geoCode2Country(lats[x], lngs[x])
			print "%s, %s = %s" %(lats[x], lngs[x], country)
			if country not in countryCount.keys():
				countryCount.update({country: 1})
			else:
				countryCount[country] += 1
				if countryCount[country] >= len(lats) / 2:	# For speed up
					return [country]
		except GeocoderTimedOut:
			print "(%f, %f) time out. Continue." %(lats[x], lngs[x])
			
	if len(countryCount) > 0:
		sortedCountry = sorted(countryCount, key=countryCount.get)
		print sortedCountry
		return [sortedCountry[-1]]
	else:
		return ''
		
def returnAllFilter(lats, lngs):
	try:
		if len(lats) <> len(lngs):
			raise IndexError("len(latitudes) deos not equal to len(longitudes)")
	except TypeError:
		print "invalid input"
		return ''
	points = []
	for x in xrange(len(lats)):
		points.append((lats[x],lngs[x]))
	return points
	
""" Return a list of 'isTaiwan' boolean values"""
def isTaiwanFilter(lats, lngs):
	results = []
	try:
		for x in xrange(len(lats)):
			results.append(isTaiwan(lats[x], lngs[x]))
	except IndexError:
		print "len(latitudes) deos not equal to len(longitudes)."
	except TypeError:
		print "Invalid input"
		return ''
	finally:
		return results

def getDefaultFilter():
	return returnAllFilter