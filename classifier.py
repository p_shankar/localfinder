#!/usr/bin/python2
""" Classifiers for 'slice' method.
@ prototype:
	result = classifier(groups_of_points, location_point, [s])
@ arguments:
	groups_of_medias - Contains all instagram `media` of an user in type `list`.
	location_point - Geography code of the location in type `tuple`.
	s - The distance to say two points are 'in the same living area'. Default 
		value set 100.
@ returning:
	True - local
	False - non-local/traveller
"""
from geopy.distance import distance
from determine import findCentroidDetermine as findCentroid

# GLOBAL
s = 100		# default

def getDefault():
	return counting

""" Merge groups nearby into as a 'Merged Group'. And calculate times that the
 user went there. 
"""
def counting(groups, location_point, _s=100, **kwargs):
	# Step 0. Set global s
	global s 
	s = _s
	# Step 1. Find centroid of all groups
	centroids = []
	for group in groups:
		centroids.append(findCentroid(group))
	# Step 2. Merge groups with their centroid close enough (distance < s).
	mergedGroups = []
	#mergedGroupID = []
	for x in xrange(len(groups)):
		centroid = centroids[x]
		mergedID = belongsTo(centroid, mergedGroups)
		# New MergedGroup
		if mergedID == -1:
			tmp = MergedGroup(centroid)
			tmp.addGroup(groups[x])
			mergedGroups.append(tmp)
		else:
			mergedGroups[mergedID].addGroup(groups[x])
	""" Step 3. Count number of groups in every 'Merged Group', which is the 
	frequency the user pass by. """
	maxLen = 0
	maxID = 0
	for mergedGroup in mergedGroups:
		if len(mergedGroup) > maxLen:
			maxID = mergedGroups.index(mergedGroup)
			maxLen = len(mergedGroups)
	""" Step 4. Check if 'location' and 'centroid of the user's living area'
	are close enough.
	"""
	centroid = findCentroid(mergedGroups[maxID].getAllPoints())
	return (distance(centroid, location_point).km < s)

""" 
Check if point belongs to a group, where point is a (lat, lng) tuple.
@ Return: 
	Integer greater than 0 -> group id 
	-1	-> Not in any group.
"""
def belongsTo(point, mergedGroups):
	global s
	if len(mergedGroups) == 0:
		return -1
	for x in xrange(len(mergedGroups)):
		group = mergedGroups[x]
		if distance(group.center, point).km < s:
			return x
	# If point is not in any group
	return -1

class MergedGroup():
	def __init__(self, center):
		self.center = center
		self.groups = []
	
	def __repr__(self):
		return "MergedGroup(%s)" %str(self.center)
		
	def __str__(self):
		return "MergedGroup(" + str(self.center) + ") -\n" + str(self.groups)
		
	def __len__(self):
		return len(self.groups)
		
	def addGroup(self, group):
		self.groups.append(group)
	
	def getAllPoints(self):
		all = []
		for group in self.groups:
			all += group
		return all
		
		